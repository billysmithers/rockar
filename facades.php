<?php

use Illuminate\Config\Repository;
use Illuminate\Contracts\Container\BindingResolutionException;

class Config
{
    protected static $instance;

    /**
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public static function __callStatic(string $method, array $args)
    {
        if (! static::$instance) {
            static::$instance = new Repository(
                (new self)->loadConfig(__DIR__ . '/config')
            );
        }

        return static::$instance->{$method}(...$args);
    }

    /**
     * @param string $configPath
     * @return array
     */
    public function loadConfig(string $configPath): array
    {
        $items = [];

        foreach(new DirectoryIterator($configPath) as $file) {
            if ($file->getExtension() === 'php') {
                $filename = str_replace('.php', '', $file->getFilename());
                $items[$filename] = require $configPath . '/' . $file->getFilename();
            }
        }

        return $items;
    }
}

class Log
{
    /**
     * @param string $name
     * @param array $args
     * @return mixed
     * @throws BindingResolutionException
     */
    public static function __callStatic(string $name, array $args)
    {
        global $container;

        $log = $container->make('log');

        return $log->{$name}(...$args);
    }
}
