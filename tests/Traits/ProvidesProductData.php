<?php

declare(strict_types=1);

namespace Tests\Traits;

trait ProvidesProductData
{
    public function provideCount()
    {
        return [
            'no parameters' => [
                [],
                0,
                6,
            ],
            'only vin WVGCV7AX7AW000784' => [
                [
                    'vin' => 'WVGCV7AX7AW000784',
                ],
                0,
                1
            ],
            'only colour White' => [
                [
                    'colour' => 'White',
                ],
                0,
                1
            ],
            'only make Jaguar' => [
                [
                    'make' => 'Jaguar',
                ],
                0,
                1
            ],
            'only price 52000 (as a string)' => [
                [
                    'price' => '52000',
                ],
                0,
                1
            ],
            'only make Jaguar and colour White should return no results' => [
                [
                    'make'   => 'Jaguar',
                    'colour' => 'White',
                ],
                0,
                0
            ],
            'colour Black should return multiple results' => [
                [
                    'colour' => 'Black',
                ],
                0,
                2
            ],
        ];
    }

    public function provideData()
    {
        $records = [
            [
                'vin'    => 'WVGCV7AX7AW000784',
                'colour' => 'Red',
                'make'   => 'Ford',
                'model'  => 'Fiesta',
                'price'  => 10000,
            ],
            [
                'vin'    => '5TBRV54138S478794',
                'colour' => 'Green',
                'make'   => 'Mitsubishi',
                'model'  => 'Eclipse',
                'price'  => 35000,
            ],
            [
                'vin'    => 'SALSH23447A102751',
                'colour' => 'Blue',
                'make'   => 'BMW',
                'model'  => '4 Series',
                'price'  => '22000',
            ],
            [
                'vin'    => '1G6DP567X50115827',
                'colour' => 'Black',
                'make'   => 'Jaguar',
                'model'  => 'XE',
                'price'  => 43000,
            ],
            [
                'vin'    => '1C6RR6LT9DS578427',
                'colour' => 'White',
                'make'   => 'Landrover',
                'model'  => 'Evoque',
                'price'  => 52000,
            ],
            [
                'vin'    => '1G6DP567X50115828',
                'colour' => 'Black',
                'make'   => 'BMW',
                'model'  => '5 Series',
                'price'  => 23000,
            ],
        ];

        return [
            'no parameters and all fields' => [
                [],
                [
                    'vin',
                    'colour',
                    'make',
                    'model',
                    'price',
                ],
                0,
                10,
                $records,
            ],
            'only vin WVGCV7AX7AW000784 and all fields' => [
                [
                    'vin' => 'WVGCV7AX7AW000784',
                ],
                [
                    'vin',
                    'colour',
                    'make',
                    'model',
                    'price',
                ],
                0,
                10,
                [
                    $records[0],
                ]
            ],
            'only colour White and all fields' => [
                [
                    'colour' => 'White',
                ],
                [
                    'vin',
                    'colour',
                    'make',
                    'model',
                    'price',
                ],
                0,
                10,
                [
                    $records[4],
                ]
            ],
            'only make Jaguar and fields vin and model' => [
                [
                    'make' => 'Jaguar',
                ],
                [
                    'vin',
                    'model',
                ],
                0,
                10,
                [
                    [
                        'vin'   => $records[3]['vin'],
                        'model' => $records[3]['model'],
                    ],
                ]
            ],
            'only price 52000 (as a string) and fields vin and model' => [
                [
                    'price' => '52000',
                ],
                [
                    'vin',
                    'model',
                ],
                0,
                10,
                [
                    [
                        'vin'   => $records[4]['vin'],
                        'model' => $records[4]['model'],
                    ],
                ]
            ],
            'only make Jaguar and colour White should return no results' => [
                [
                    'make'   => 'Jaguar',
                    'colour' => 'White',
                ],
                [
                    'vin',
                    'colour',
                    'make',
                    'model',
                    'price',
                ],
                0,
                10,
                []
            ],
            'only make Jaguar and fields vin and model but not results as offset set past maximum result' => [
                [
                    'make' => 'Jaguar',
                ],
                [
                    'vin',
                    'model',
                ],
                6,
                10,
                [
                ]
            ],
            'colour Black should return multiple results' => [
                [
                    'colour' => 'Black',
                ],
                [
                    'vin',
                    'colour',
                    'make',
                    'model',
                    'price',
                ],
                0,
                10,
                [
                    $records[3],
                    $records[5],
                ]
            ],
        ];
    }
}