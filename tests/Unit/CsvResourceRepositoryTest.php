<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\Exception\ResourceException;
use App\Repository\CsvResourceRepository;
use PHPUnit\Framework\TestCase;
use Tests\Traits\ProvidesProductData;

final class CsvResourceRepositoryTest extends TestCase
{
    use ProvidesProductData;

    private CsvResourceRepository $repository;

    public function setUp(): void
    {
        $this->repository = new CsvResourceRepository();
        $this->repository->setPath(
            dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'storage'
            . DIRECTORY_SEPARATOR . 'fixtures' . DIRECTORY_SEPARATOR . 'product.csv'
        );
    }

    /**
     * @dataProvider provideCount
     * @param array $parameters
     * @param int $offset
     * @param int $count
     * @throws ResourceException
     */
    public function testCanProvideCountFromCsvAccountingForParameters(
        array $parameters,
        int $offset,
        int $count
    ): void {
        $this->assertEquals(
            $count,
            $this->repository->countByParameters($parameters, $offset)
        );
    }

    public function testCountThrowsOutOfBoundsException(): void {
        $this->expectException(ResourceException::class);

        $this->repository->countByParameters([], 7);
    }

    /**
     * @dataProvider provideData
     * @param array $parameters
     * @param array $fields
     * @param int $offset
     * @param int $limit
     * @param array $filtered
     * @throws ResourceException
     */
    public function testCanReadDataFromCsvAccountingForParameters(
        array $parameters,
        array $fields,
        int $offset,
        int $limit,
        array $filtered
    ): void {
        $this->assertEquals(
            $filtered,
            $this->repository->byParameters($parameters, $fields, $offset, $limit)
        );
    }
}
