<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\Http\Response\ResourceResponse;
use PHPUnit\Framework\TestCase;

final class ResourceResponseTest extends TestCase
{
    public function testNextUrlIsNull(): void {
        $resourceResponse = new ResourceResponse(5, 0, 10, [], []);

        $this->assertNull($resourceResponse->getData()->next);
    }

    public function testNextUrlWithNoParams(): void {
        $_SERVER['REQUEST_URI'] = '/product/';
        $_SERVER['HTTP_HOST']   = 'localhost';

        $resourceResponse = new ResourceResponse(20, 0, 10, [], []);

        $this->assertEquals(
            'http://localhost/product/?offset=10&limit=10',
            $resourceResponse->getData()->next
        );
    }

    public function testNextUrlWithExistingParams(): void {
        $_SERVER['REQUEST_URI'] = '/product/';
        $_SERVER['HTTP_HOST']   = 'localhost';

        $resourceResponse = new ResourceResponse(
            20,
            0,
            10,
            [],
            [
                'vin'    => '1234567890',
                'colour' => 'Black',
            ]
        );

        $this->assertEquals(
            'http://localhost/product/?offset=10&limit=10&vin=1234567890&colour=Black',
            $resourceResponse->getData()->next
        );
    }
}
