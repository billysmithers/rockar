<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\Exception\ResourceException;
use App\Repository\DbProductRepository;
use PHPUnit\Framework\TestCase;
use Tests\Traits\ProvidesProductData;

final class DbProductRepositoryTest extends TestCase
{
    use ProvidesProductData;

    private DbProductRepository $productRepository;

    public function setUp(): void
    {
        $this->productRepository = new DbProductRepository();
        $this->productRepository->setPath(
            dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'storage'
            . DIRECTORY_SEPARATOR . 'fixtures' . DIRECTORY_SEPARATOR . 'product.csv'
        );
    }

    /**
     * @dataProvider provideCount
     * @param array $parameters
     * @param int $offset
     * @param int $count
     * @throws ResourceException
     */
    public function testCanProvideCountFromDbAccountingForParameters(
        array $parameters,
        int $offset,
        int $count
    ): void {
        $this->assertEquals(
            $count,
            $this->productRepository->countByParameters($parameters, $offset)
        );
    }

    public function testCountThrowsOutOfBoundsException(): void {
        $this->expectException(ResourceException::class);

        $this->productRepository->countByParameters([], 7);
    }

    /**
     * @dataProvider provideData
     * @param array $parameters
     * @param array $fields
     * @param int $offset
     * @param int $limit
     * @param array $filtered
     * @throws ResourceException
     */
    public function testCanReadDataFromDbAccountingForParameters(
        array $parameters,
        array $fields,
        int $offset,
        int $limit,
        array $filtered
    ): void {
        $this->assertEquals(
            $filtered,
            $this->productRepository->byParameters($parameters, $fields, $offset, $limit)
        );
    }
}
