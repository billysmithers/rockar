<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\Exception\ResourceException;
use App\Repository\DbCustomerRepository;
use PHPUnit\Framework\TestCase;

final class DbCustomerRepositoryTest extends TestCase
{
    private DbCustomerRepository $productRepository;

    public function setUp(): void
    {
        $this->productRepository = new DbCustomerRepository();
        $this->productRepository->setPath(
            dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'storage'
            . DIRECTORY_SEPARATOR . 'fixtures' . DIRECTORY_SEPARATOR . 'customer.csv'
        );
    }

    /**
     * @dataProvider provideCount
     * @param array $parameters
     * @param int $offset
     * @param int $count
     * @throws ResourceException
     */
    public function testCanProvideCountFromDbAccountingForParameters(
        array $parameters,
        int $offset,
        int $count
    ): void {
        $this->assertEquals(
            $count,
            $this->productRepository->countByParameters($parameters, $offset)
        );
    }

    public function testCountThrowsOutOfBoundsException(): void {
        $this->expectException(ResourceException::class);

        $this->productRepository->countByParameters([], 7);
    }

    /**
     * @dataProvider provideData
     * @param array $parameters
     * @param array $fields
     * @param int $offset
     * @param int $limit
     * @param array $filtered
     * @throws ResourceException
     */
    public function testCanReadDataFromDbAccountingForParameters(
        array $parameters,
        array $fields,
        int $offset,
        int $limit,
        array $filtered
    ): void {
        $this->assertEquals(
            $filtered,
            $this->productRepository->byParameters($parameters, $fields, $offset, $limit)
        );
    }

    public function provideCount()
    {
        return [
            'no parameters' => [
                [],
                0,
                3,
            ],
            'only email tom.harding1974@gmail.co.uk' => [
                [
                    'email' => 'tom.harding1974@gmail.co.uk',
                ],
                0,
                1
            ],
            'only forename Tom' => [
                [
                    'forename' => 'Tom',
                ],
                0,
                1
            ],
            'only surname Sutton' => [
                [
                    'surname' => 'Sutton',
                ],
                0,
                1
            ],
            'only contact number +44 (0) 7950 244 036' => [
                [
                    'contact_number' => '+44 (0) 7950 244 036',
                ],
                0,
                1
            ],
            'only forename Tom and surname Sutton should return no results' => [
                [
                    'forename' => 'Tom',
                    'surname'  => 'Sutton',
                ],
                0,
                0
            ],
        ];
    }

    public function provideData()
    {
        $records = [
            [
                'email'          => 'tom.harding1974@gmail.co.uk',
                'forename'       => 'Tom',
                'surname'        => 'Harding',
                'contact_number' => '07938244758',
                'postcode'       => 'SS26GH',
            ],
            [
                'email'          => 'drosmanahmed@pharmaceuticalsglobal.org',
                'forename'       => 'Osman',
                'surname'        => 'Ahmed',
                'contact_number' => '+91719548839',
                'postcode'       => '396210',
            ],
            [
                'email'          => 'dominic.sutton@rockar.com',
                'forename'       => 'Dominic',
                'surname'        => 'Sutton',
                'contact_number' => '+44 (0) 7950 244 036',
                'postcode'       => 'W12 7SL',
            ],
        ];

        return [
            'no parameters and all fields' => [
                [],
                [
                    'email',
                    'forename',
                    'surname',
                    'contact_number',
                    'postcode',
                ],
                0,
                10,
                $records,
            ],
            'only email tom.harding1974@gmail.co.uk and all fields' => [
                [
                    'email' => 'tom.harding1974@gmail.co.uk',
                ],
                [
                    'email',
                    'forename',
                    'surname',
                    'contact_number',
                    'postcode',
                ],
                0,
                10,
                [
                    $records[0],
                ]
            ],
            'only forename Tom and all fields' => [
                [
                    'forename' => 'Tom',
                ],
                [
                    'email',
                    'forename',
                    'surname',
                    'contact_number',
                    'postcode',
                ],
                0,
                10,
                [
                    $records[0],
                ]
            ],
            'only surname Sutton and fields forename and surname' => [
                [
                    'surname' => 'Sutton',
                ],
                [
                    'forename',
                    'surname',
                ],
                0,
                10,
                [
                    [
                        'forename' => $records[2]['forename'],
                        'surname'  => $records[2]['surname'],
                    ],
                ]
            ],
            'only contact number +44 (0) 7950 244 036 and fields forename and surname' => [
                [
                    'contact_number' => '+44 (0) 7950 244 036',
                ],
                [
                    'forename',
                    'surname',
                ],
                0,
                10,
                [
                    [
                        'forename' => $records[2]['forename'],
                        'surname'  => $records[2]['surname'],
                    ],
                ]
            ],
            'only forename Tom and surname Sutton should return no results' => [
                [
                    'forename' => 'Tom',
                    'surname'  => 'Sutton',
                ],
                [
                    'email',
                    'forename',
                    'surname',
                    'contact_number',
                    'postcode',
                ],
                0,
                10,
                []
            ],
            'only surname Sutton and fields forename and surname but not results as offset set past maximum result' => [
                [
                    'surname' => 'Sutton',
                ],
                [
                    'forename',
                    'surname',
                ],
                4,
                10,
                [
                ]
            ],
        ];
    }
}
