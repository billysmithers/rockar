<?php

declare(strict_types=1);

namespace App\Http\Request;

class CustomerRequest extends ResourceRequest
{
    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            'email'          => 'string',
            'forename'       => 'string',
            'surname'        => 'string',
            'contact_number' => 'string',
            'postcode'       => 'string',
            'offset'         => 'integer',
            'limit'          => 'integer',
        ];
    }

    public function setUpForRepository(): void
    {
        $this->parameters = $this->only(['email', 'forename', 'surname', 'contact_number', 'postcode']);
        $this->fields     = explode(
            ',',
            $this->input('fields', 'email,forename,surname,contact_number,postcode')
        );
        $this->offset     = (int) $this->input('offset', 0);
        $this->limit      = (int) $this->input('limit', 10);
    }
}
