<?php

declare(strict_types=1);

namespace App\Http\Request;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Response;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\Validator;

abstract class BaseApiRequest
{
    /**
     * @var string[]
     */
    private array $messages;

    /**
     * @var Request
     */
    protected Request $request;

    /**
     * @var Factory
     */
    private $validator;

    /**
     * @var Validator
     */
    private Validator $validation;

    /**
     * @param array $data
     * @return bool
     */
    public function isInvalid(array $data = [])
    {
        return ! $this->isValid($data);
    }

    /**
     * @return array
     */
    protected abstract function rules(): array;

    /**
     * @param array $data
     * @return bool
     */
    private function isValid(array $data = []): bool
    {
        if (! $this->validator) {
            $loader          = new FileLoader(new Filesystem, 'lang');
            $translator      = new Translator($loader, 'en');
            $this->validator = new Factory($translator);
            $this->request   = Request::capture();
            $this->messages  = require dirname(dirname(dirname(__DIR__))) . '/lang/en/validation.php';
        }

        $this->validation = $this->validator->make(
            array_merge($this->request->all(), $data),
            $this->rules(),
            $this->messages
        );

        if (! $this->validation->fails()) {
            return true;
        }

        return false;
    }

    /**
     * @return JsonResponse
     */
    public function invalidResponse(): JsonResponse
    {
        return new JsonResponse(
            [
                'errors' => $this->validation->errors()
            ],
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->request->{$name}(...$arguments);
    }
}