<?php

declare(strict_types=1);

namespace App\Http\Request;

class ProductRequest extends ResourceRequest
{
    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            'vin'    => 'string',
            'colour' => 'string',
            'make'   => 'string',
            'model'  => 'string',
            'price'  => 'integer',
            'offset' => 'integer',
            'limit'  => 'integer',
        ];
    }

    public function setUpForRepository(): void
    {
        $this->parameters = $this->only(['vin', 'colour', 'make', 'model', 'price']);
        $this->fields     = explode(',', $this->input('fields', 'vin,colour,make,model,price'));
        $this->offset     = (int) $this->input('offset', 0);
        $this->limit      = (int) $this->input('limit', 10);
    }
}
