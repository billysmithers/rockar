<?php

declare(strict_types=1);

namespace App\Http\Request;

abstract class ResourceRequest extends BaseApiRequest
{
    /**
     * @var array
     */
    protected array $parameters;

    /**
     * @var array
     */
    protected array $fields;

    /**
     * @var int
     */
    protected int $offset;

    /**
     * @var int
     */
    protected int $limit;

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    public abstract function setUpForRepository(): void;
}
