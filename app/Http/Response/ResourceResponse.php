<?php

declare(strict_types=1);

namespace App\Http\Response;

use Illuminate\Http\JsonResponse;

class ResourceResponse extends JsonResponse
{
    public function __construct(
        int $count,
        int $offset,
        int $limit,
        array $results,
        array $params,
        int $status = JsonResponse::HTTP_OK
    ) {
        parent::__construct(
            [
                'count' => $count,
                'next'  => $this->buildNext($count, $offset, $limit, $params),
                'data'  => $results,
            ],
            $status
        );
    }

    /**
     * @param int $count
     * @param int $offset
     * @param int $limit
     * @param array $params
     * @return string|null
     */
    private function buildNext(int $count, int $offset, int $limit, array $params): ?string
    {
        if (($offset + $limit) >= $count) {
            return null;
        }

        $protocol    = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http');
        $parts       = parse_url($_SERVER['REQUEST_URI']);
        $newOffset   = $offset + $limit;
        $queryParams = array_merge(
            [
                'offset' => $newOffset,
                'limit'  => $limit,
            ],
            $params
        );

        return $protocol . '://' . $_SERVER['HTTP_HOST'] . $parts['path'] . '?' . http_build_query($queryParams);
    }
}