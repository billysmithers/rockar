<?php

declare(strict_types=1);

namespace App\Http\Controller;

use App\Exception\ResourceException;
use App\Factory\CustomerRepositoryFactory;
use App\Http\Request\CustomerRequest;
use App\Http\Response\ResourceResponse;
use Config;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class CustomerController extends Controller
{
    /**
     * @param CustomerRequest $request
     * @param CustomerRepositoryFactory $factory
     * @return JsonResponse
     */
    public function search(
        CustomerRequest $request,
        CustomerRepositoryFactory $factory
    ): JsonResponse {
        if ($request->isInvalid()) {
            return $request->invalidResponse();
        }

        try {
            $request->setUpForRepository();

            $customers = $factory->make(Config::get('customer.datasource'));
            $count     = $customers->countByParameters($request->getParameters(), $request->getOffset());
            $results   = $customers->byParameters(
                $request->getParameters(),
                $request->getFields(),
                $request->getOffset(),
                $request->getLimit()
            );

            return new ResourceResponse(
                $count,
                $request->getOffset(),
                $request->getLimit(),
                $results,
                $request->getParameters()
            );
        } catch (ResourceException $e) {
            return new JsonResponse(['errors' => $e->getMessage()], $e->getCode());
        }
    }
}
