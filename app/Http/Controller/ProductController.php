<?php

declare(strict_types=1);

namespace App\Http\Controller;

use App\Exception\ResourceException;
use App\Factory\ProductRepositoryFactory;
use App\Http\Request\ProductRequest;
use App\Http\Response\ResourceResponse;
use Config;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{
    /**
     * @param ProductRequest $request
     * @param ProductRepositoryFactory $factory
     * @return JsonResponse
     */
    public function search(
        ProductRequest $request,
        ProductRepositoryFactory $factory
    ): JsonResponse {
        if ($request->isInvalid()) {
            return $request->invalidResponse();
        }

        try {
            $request->setUpForRepository();

            $products = $factory->make(Config::get('product.datasource'));
            $count    = $products->countByParameters($request->getParameters(), $request->getOffset());
            $results  = $products->byParameters(
                $request->getParameters(),
                $request->getFields(),
                $request->getOffset(),
                $request->getLimit()
            );

            return new ResourceResponse(
                $count,
                $request->getOffset(),
                $request->getLimit(),
                $results,
                $request->getParameters()
            );
        } catch (ResourceException $e) {
            return new JsonResponse(['errors' => $e->getMessage()], $e->getCode());
        }
    }
}
