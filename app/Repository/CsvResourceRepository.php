<?php

declare(strict_types=1);

namespace App\Repository;

use App\Exception\ResourceException;
use League\Csv\Exception;
use League\Csv\Reader;
use League\Csv\Statement;
use OutOfRangeException;

class CsvResourceRepository implements ResourceRepositoryInterface
{
    private string $path;

    public function setPath(string $path)
    {
        $this->path = $path;
    }

    /**
     * @inheritDoc
     * @throws ResourceException
     */
    public function countByParameters(array $parameters, int $offset = 0): int
    {
        try {
            return $this->getCount($parameters,  $offset);
        } catch (OutOfRangeException $e) {
            throw new ResourceException($e->getMessage(), 422);
        } catch (Exception $e) {
            throw new ResourceException('Unable to gather data for requested resource, sorry.', 500);
        }
    }

    /**
     * @inheritDoc
     * @throws ResourceException
     */
    public function byParameters(array $parameters, array $fields, int $offset = 0, int $limit = 10): array
    {
        try {
            return $this->getRecords($parameters, $fields, $offset, $limit);
        } catch (Exception $e) {
            throw new ResourceException('Unable to gather data for requested resource, sorry.', 500);
        }
    }

    /**
     * @param array $parameters
     * @param int $offset
     * @return int
     * @throws OutOfRangeException|Exception
     */
    protected function getCount(array $parameters, int $offset): int
    {
        $reader  = Reader::createFromPath($this->path, 'r');
        $reader->setHeaderOffset(0);

        $stmt  = $this->createCSVStatement($parameters);
        $count = $stmt->process($reader)->count();

        if ($count > 0 && $offset >= $count) {
            throw new OutOfRangeException('The offset is greater than the count.');
        }

        return $count;
    }

    /**
     * @param array $parameters
     * @param array $fields
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws Exception
     */
    protected function getRecords(array $parameters, array $fields, int $offset, int $limit): array
    {
        $records = [];
        $reader  = Reader::createFromPath($this->path, 'r');
        $reader->setHeaderOffset(0);

        $stmt = $this->createCSVStatement($parameters)
        ->offset($offset)
        ->limit($limit);

        $results = $stmt->process($reader);

        foreach ($results as $result) {
            $records[] = array_filter($this->cleanse($result), function($key) use ($fields) {
                return in_array($key, $fields);
            }, ARRAY_FILTER_USE_KEY);
        }

        return $records;
    }

    /**
     * @param $parameters
     * @return Statement
     * @throws Exception
     */
    protected function createCSVStatement($parameters): Statement
    {
        $reader  = Reader::createFromPath($this->path, 'r');
        $reader->setHeaderOffset(0);

        return (new Statement())
            ->where(fn(array $record) => $this->isMatchingRecord($record, $parameters));
    }

    protected function isMatchingRecord(array $record, array $parameters): bool
    {
        if (empty($parameters)) {
            return true;
        }

        $cleansedParameters = $this->cleanse($parameters);
        $unmatched          = array_diff_assoc($this->cleanse($record), $cleansedParameters);

        foreach ($cleansedParameters as $key => $value) {
            if (isset($unmatched[$key])) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $original
     * @return array
     */
    protected function cleanse(array $original): array
    {
        $clean = array_map('trim', $original);

        if (! empty($original['price'])) {
            $clean['price'] = (int) $original['price'];
        }

        return $clean;
    }
}
