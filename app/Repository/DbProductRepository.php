<?php

declare(strict_types=1);

namespace App\Repository;

/**
 * In the real world this would be an Eloquent model (hence the separate Product and Customer repositories)
 * implementing the interface that would be the ORM for MySQL, Postres, GraphQL etc.
 * But as we are only mocking the real world - stub this using the CSVRepository
 */
class DbProductRepository extends CsvResourceRepository implements ResourceRepositoryInterface
{
}
