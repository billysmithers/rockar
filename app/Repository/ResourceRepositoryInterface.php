<?php

namespace App\Repository;

interface ResourceRepositoryInterface
{
    /**
     * @param array $parameters
     * @param int $offset
     * @return int
     */
    public function countByParameters(array $parameters, int $offset): int;

    /**
     * @param array $parameters
     * @param array $fields
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function byParameters(array $parameters, array $fields, int $offset, int $limit): array;
}