<?php

declare(strict_types=1);

namespace App\Factory;

use App\Repository\CsvResourceRepository;
use App\Repository\ResourceRepositoryInterface;
use App\Repository\DbCustomerRepository;
use Config;

class CustomerRepositoryFactory
{
    /**
     * @param string $dataSource
     * @return ResourceRepositoryInterface
     */
    public function make(string $dataSource): ResourceRepositoryInterface
    {
        switch ($dataSource) {
            case 'db':
                $db = new DbCustomerRepository();
                $db->setPath(Config::get('customer.csv-path'));

                return $db;
            default:
                $csv = new CsvResourceRepository();
                $csv->setPath(Config::get('customer.csv-path'));

                return $csv;
        }
    }
}
