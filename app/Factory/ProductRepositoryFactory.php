<?php

declare(strict_types=1);

namespace App\Factory;

use App\Repository\CsvResourceRepository;
use App\Repository\DbProductRepository;
use App\Repository\ResourceRepositoryInterface;
use Config;

class ProductRepositoryFactory
{
    /**
     * @param string $dataSource
     * @return ResourceRepositoryInterface
     */
    public function make(string $dataSource): ResourceRepositoryInterface
    {
        switch ($dataSource) {
            case 'db':
                $db = new DbProductRepository();
                $db->setPath(Config::get('product.csv-path'));

                return $db;
            default:
                $csv = new CsvResourceRepository();
                $csv->setPath(Config::get('product.csv-path'));

                return $csv;
        }
    }
}
