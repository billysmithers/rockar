<?php

declare(strict_types=1);

namespace App\Support;

use Log;
use Throwable;

class ErrorHandler
{
    public function __construct()
    {
        $this->registerHandlers();
    }
    
    private function registerHandlers()
    {
        $this->registerLocalExceptionHandler();
        $this->registerLocalErrorHandler();
        $this->registerShutdownFunction();
    }

    private function registerShutdownFunction()
    {
        register_shutdown_function(function() {
            $error = error_get_last();

            if(! is_null($error) && $error['type'] === E_ERROR) {
                Log::error('PHP Fatal Error: ' . json_encode($error));
            }
        });
    }
    
    private function registerLocalExceptionHandler()
    {
        set_exception_handler(function(Throwable $e) {
            Log::error('PHP Exception: ' . $e->getMessage() . ': ' . $e->getTraceAsString());
        });
    }
    
    private function registerLocalErrorHandler()
    {
        set_error_handler(function($errorNumber, $errorString, $errorFile, $errorLine) {
            switch ($errorNumber) {
                case E_NOTICE:
                case E_USER_NOTICE:
                    $error = 'Notice';
                    break;
                case E_WARNING:
                case E_USER_WARNING:
                    $error = 'Warning';
                    break;
                case E_ERROR:
                case E_USER_ERROR:
                    $error = 'Fatal Error';
                    break;
                case E_DEPRECATED:
                    $error = '';
                    break;
                default:
                    $error = 'Unknown';
                    break;
            }

            if ($error) {
                Log::error(
                    'PHP ' . $error . ':  ' . $errorString . ' in ' . $errorFile . ' on line ' . $errorLine
                );
            }

            return true;
        });
    }
}