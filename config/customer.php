<?php

return [
    'datasource' => env('CUSTOMER_API_DATASOURCE', 'csv'),
    'csv-path'   => dirname(__DIR__) . DIRECTORY_SEPARATOR . 'storage'
        . DIRECTORY_SEPARATOR . 'fixtures' . DIRECTORY_SEPARATOR . 'customer.csv'
];
