<?php

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Router;

$router->group([], function (Router $router) {
    $router->group([
        'namespace' => 'App\Http\Controller',
        'prefix'    => 'product'
    ], function (Router $router) {
        $router->get(
            '/',
            [
                'name' => 'product.search',
                'uses' => 'ProductController@search'
            ]
        );
    });
});

$router->group([], function (Router $router) {
    $router->group([
        'namespace' => 'App\Http\Controller',
        'prefix'    => 'customer'
    ], function (Router $router) {
        $router->get(
            '/',
            [
                'name' => 'customer.search',
                'uses' => 'CustomerController@search'
            ]
        );
    });
});

$router->any('{any}', function () {
    return new JsonResponse(['name' => 'Rockar Tech Test API', 'version' => '1.0']);
})->where('any', '(.*)');
