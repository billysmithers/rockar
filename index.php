<?php

require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/facades.php');

use App\Support\ErrorHandler;
use Dotenv\Dotenv;
use Illuminate\Config\Repository;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Log\Logger;
use Illuminate\Http\Request;
use Illuminate\Routing\Pipeline;
use Illuminate\Routing\Router;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger as MonologLogger;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

$container = new Container;

/** Config */
$container['config'] = new Repository(Config::loadConfig(__DIR__ . '/config'));

/** Logging */
$container->singleton('log', function () {
    $log = new Logger(new MonologLogger('Rockar Tech Test API'));

    $log->pushHandler(new RotatingFileHandler(__DIR__ . '/storage/logs/rockar-tech-test-api.log', 7));

    return $log;
});

/** Errors */
new ErrorHandler();

/** Events */
$container->singleton('events', function ($container) {
    return new Dispatcher($container);
});

$events = $container->get('events');

$router = new Router($events);

require_once(__DIR__ . '/routes/api.php');

$request  = Request::capture();
$response = (new Pipeline($container))
    ->send($request)
    ->then(function ($request) use ($router) {
        return $router->dispatch($request);
    });

$response->send();
