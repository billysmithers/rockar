# Billy Smithers Rockar API (Tech Test)

## Requirements

* PHP 7.4 (No features to warrant PHP8)
* Composer

## Installation and running

Run composer install and run the built in PHP web server from a port of your choice
(example uses port 8000)

```
composer install
```

```
php -S localhost:8000
```

## Tests

```
./vendor/bin/phpunit
```

## API

### Product

#### Request

```
GET http://localhost:8000/product/

query params

vin
colour
make
model
price
fields (comma separated list)
offset
limit

Example:

http://localhost:8000/product/?vin=WVGCV7AX7AW000784&fields=make,model
```

#### Response

```json
{
  "count": 5,
  "next": "http://localhost:8000/product/?offset=2&fields=make,model",
  "data": [
      {
         "make": "Ford",
         "model": "Fiesta"
      }
   ] 
}
```

### Customer

#### Request

```
GET http://localhost:8000/customer/

query params

email
forename
surname
contact_number
postcode
fields (comma separated list)
offset
limit

Example:

http://localhost:8000/customer/?surname=Sutton&fields=forename,postcode
```

#### Response

```json
{
  "count": 5,
  "next": "http://localhost:8000/customer/?offset=2&fields=forename,postcode",
  "data": [
      {
         "forename": "Dominic",
         "postcode": "W12 7SL"
      }
   ] 
}
```
## Reasoning behind approach

Great challenge!

I chose to keep the endpoints separate as in reality out in the wild these would probably be separate customer and product microservices.
If we wanted to do something like get a customers orders with products we would use an API Gateway or CQRS approach. This would mean
querying the customer, product and order APIs respectively and aggregating the response for the frontend.

I chose a lightweight framework build based on Laravel Illuminate packages, as I wanted the test app to be lightweight and felt
Lumen or Laravel was overkill in order to achieve the requirements of the test.

I mocked / stubbed the Db Repos to extend / use the CSV Repo. In reality in the real world this would probably be Eloquent models
that would be the ORM for MySQL, Postgres, GraphQL etc. Hence the DB repos being separate Product and Customer repositories.
They would be easy to swap out (implement the ``ResourceRepositoryInterface`` and update the factories.)

Although not in the design of the API response I thought it would be nice to implement a count (total resources) and next as part of the response.

I know the design also said the data to be an object but in the case of multiple matches I have made this an array of objects.
I added an extra line to the product CSV to demonstrate this.

I also noticed one of the customer lines had an invalid UK postcode, if that was intentional I have left in place as IMHO that should be handled
in the validation of saving the record / resource.

[I have used Git semantic messaging for my commits](https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716).
